
variable "admin_cidr_block" {
  #default = ["76.103.227.0/24", "166.177.248.0/24"]
  default = "0.0.0.0/0"
}

variable "domain_name" {
  default = "uxlabs.net"
}

variable "bvoss_pubkey" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJ4/Whrp1eqPCMAH1tm5zD6wqK0c54hoYhSY0k3KO/5kUATAs22xs5W80NypJc62dXOlLuDhzQd/1t1SL0MzgXNxvV1uv7Gedv9tUApL/AP3EGLIY81vAvP7hOlyrdxpzHN5M0zlm/DYPJxIkT29B14hD4sugevWIcaWpPexD3NBZpcKUPVQMuSl9jRl2enuOOw8izw2vLSKR0bag/khkR/d8BLuCyRxfZdwMhKTPbF+3VF1z6DpkkEFVRD8KfOIo1JrrKSqsV7L4H9981xV9y7or4BoTYdvXYg5HOLYpqKuV9r/Kc+B31MwDJFPQY6KW5w6ZeZInnwjV6M3VeNbD9 brianvoss@Brians-Air"
 }

 variable "automation_pubkey" {
   default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBzMwjxOsCLf4JY0VZy9AWtkfy9t5IfawcOM5r8/VxvBONhK0ldV515XWnwP41viMgoFCQ0KxaUa8f1iroMoM/KaF/QajNcva4Q1fV/pdn0cXd32Di5ggYtFTtYk+Mc3uP70dviwff4lF1av5sJ5J84VN6hepRBdCiRCgIU5f9HvWYbLbK9BFW1YLZk9PfWFhGG3+iQiw6AK5T3bqONhisiXTUwu5NJrphV/xhUHk24gOBQVsx1xBZS1UUiIyhaaQl7PMea4fMNA8pSqEAEPyY5mgvL4A0SoKzxEzNzQJ6x/e2vaqgtYEZfznAcPvIZrrOPtnNEzLKFpqiYDtW5hRb brianvoss@Brians-Air"
 }