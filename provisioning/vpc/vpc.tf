
data "aws_availability_zones" "available" {}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name                 = "${var.service}"
  cidr                 = "10.10.10.0/24"

  azs                  = ["${data.aws_availability_zones.available.names[0]}","${data.aws_availability_zones.available.names[1]}","${data.aws_availability_zones.available.names[2]}"]
  private_subnets      = ["10.10.10.0/25"]
  public_subnets       = ["10.10.10.128/25"]
  enable_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform = "true"
    Environment = "${var.environment}"
    service = "${var.service}"
  }
}