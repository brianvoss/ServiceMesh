
variable "environment" {
  default = "prod"
}

variable "service" {
  default = "servicemesh"
}

variable "service_prefix" {
  default = "vpc"
}