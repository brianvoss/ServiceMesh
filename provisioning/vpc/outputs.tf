output "environment" {
	value = "${var.environment}"
}

output "public_domain_name" {
  value = "${var.domain_name}"
}

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "vpc_cidr_block" {
  value = "${module.vpc.vpc_cidr_block}"
}

output "default_security_group_id" {
  value = "${module.vpc.default_security_group_id}"
}

output "default_network_acl_id" {
  value = "${module.vpc.default_network_acl_id}"
}

# Subnets
output "private_subnets" {
  value = "${module.vpc.private_subnets}"
}

output "public_subnets" {
  value = "${module.vpc.public_subnets}"
}

output "database_subnets" {
  value = "${module.vpc.database_subnets}"
}

output "database_subnet_group" {
  value = "${module.vpc.database_subnet_group}"
}

output "elasticache_subnets" {
  value = "${module.vpc.elasticache_subnets}"
}

output "elasticache_subnet_group" {
  value = "${module.vpc.elasticache_subnet_group}"
}

# Route tables
output "public_route_table_ids" {
  value = "${module.vpc.public_route_table_ids}"
}

output "private_route_table_ids" {
  value = "${module.vpc.private_route_table_ids}"
}

output "nat_ids" {
  value = "${module.vpc.nat_ids}"
}

output "nat_public_ips" {
  value = "${module.vpc.nat_public_ips}"
}

output "natgw_ids" {
  value = "${module.vpc.natgw_ids}"
}

# Internet Gateway
output "igw_id" {
  value = "${module.vpc.igw_id}"
}

# VPC Endpoints
output "vpc_endpoint_s3_id" {
  value = "${module.vpc.vpc_endpoint_s3_id}"
}

output "vpc_endpoint_dynamodb_id" {
  value = "${module.vpc.vpc_endpoint_dynamodb_id}"
}

output "bvoss_pubkey" {
  value = "${var.bvoss_pubkey}"
}

output "automation_pubkey" {
  value = "${var.automation_pubkey}"
}

output "admin_cidr_block" {
  value = "${var.admin_cidr_block}"
}