terraform {
  backend "s3" {
    bucket = "servicemesh-terraform-state"
    key    = "servicemesh/vpc/terraform"
    region = "us-west-2"
  }
}