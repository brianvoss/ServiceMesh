resource "aws_key_pair" "bvoss" {
  key_name = "bvoss"
  public_key = "${var.bvoss_pubkey}"
}

resource "aws_key_pair" "automation" {
  key_name = "automation"
  public_key = "${var.automation_pubkey}"
}