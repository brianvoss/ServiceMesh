resource "aws_security_group" "traefik" {
  name        = "traefik"
  description = "Traefik Security Group"
  vpc_id      = "${data.terraform_remote_state.base.vpc_id}"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = "${var.zg_allow_cidr_blocks}"
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.zg_allow_cidr_blocks}"
  }
  ingress {
    from_port   = 8088
    to_port     = 8088
    protocol    = "tcp"
    cidr_blocks = "${var.zg_allow_cidr_blocks}"
  }
  ingress {
    from_port   = 2112
    to_port     = 2112
    protocol    = "tcp"
    cidr_blocks = "${var.zg_allow_cidr_blocks}"
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.zg_allow_cidr_blocks}"
  }
  ingress {
    from_port   = 1024
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [ "${data.terraform_remote_state.base.vpc_cidr_block}" ]
  }
  ingress {
    from_port   = 1024
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = [ "${data.terraform_remote_state.base.vpc_cidr_block}" ]
  }
  egress {
    from_port   = 6800
    to_port     = 6800
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  egress {
    from_port   = 1024
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
}

resource "aws_instance" "traefik" {
  ami               = "${data.aws_ami.traefik.id}"
  instance_type     = "t2.small"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  subnet_id         = "${data.terraform_remote_state.base.public_subnets}"
  vpc_security_group_ids = ["${aws_security_group.traefik.id}"]
  source_dest_check = false
  key_name          = "${data.terraform_remote_state.base.key_name}"
  tags {
    Name = "${var.environment}-traefik"
  }
  user_data         = "{\"consul_master\":\"${data.terraform_remote_state.base.bastion_internal_ip}\", \"domain\": \"${data.terraform_remote_state.base.consul_domain}\", \"role\": \"traefik\", \"cluster_name\": \"${data.terraform_remote_state.base.consul_cluster_name}\"}"
}