
data "aws_ami" "app" {
  most_recent = true
  owners = ["self"]
  filter {
    name = "tag-key"
    values = ["role"]
  }
  filter {
    name = "tag-value"
    values = ["app"]
  }
}
