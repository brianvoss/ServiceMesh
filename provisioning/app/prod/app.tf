
data "aws_availability_zones" "available" {}

resource "aws_security_group" "app" {
  name        = "app-${var.environment}"
  description = "Blink Application"
  vpc_id      = "${data.terraform_remote_state.vpc.vpc_id}"
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [ "${data.terraform_remote_state.base.vpc_cidr_block}" ]
  }
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = [ "${data.terraform_remote_state.base.vpc_cidr_block}" ]
  }
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = [ "${data.terraform_remote_state.base.vpc_cidr_block}" ]
  }
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = [ "${data.terraform_remote_state.base.vpc_cidr_block}" ]
  }
}

resource "aws_launch_configuration" "app" {
  image_id        = "${data.aws_ami.app.id}"
  instance_type   = "t2.small"
  security_groups = [ "${aws_security_group.app.id}" ]
  
  lifecycle     = {
    create_before_destroy = true
  }

  user_data     = "${data.template_cloudinit_config.app.rendered}"
}

resource "aws_autoscaling_group" "app" {
  name                      = "app-asg-${var.environment}"
  availability_zones        = ["${data.aws_availability_zones.available.names[0]}"]
  max_size                  = "1"
  min_size                  = "1"
  desired_capacity          = "1"
  health_check_grace_period = 300
  health_check_type         = "EC2"
  force_delete              = true
  launch_configuration      = "${aws_launch_configuration.app.name}"
  vpc_zone_identifier       = [ "${data.terraform_remote_state.vpc.private_subnets[0]}" ]
  
  tag {
    key                 = "environment"
    value               = "${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "role"
    value               = "app"
    propagate_at_launch = true
  }
}

