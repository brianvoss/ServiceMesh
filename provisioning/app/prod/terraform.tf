terraform {
  backend "s3" {
    bucket = "acqu-terraform-state"
    key    = "sitemaps/sitemap-generator/prod/terraform"
    region = "us-west-2"
  }
}