data "aws_route53_zone" "trulia-web" {
  name         = "trulia-web.services."
  private_zone = false
}

resource "aws_route53_record" "traefik" {
  zone_id = "${data.aws_route53_zone.trulia-web.zone_id}"
  name    = "sitemaps.${data.aws_route53_zone.trulia-web.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.traefik-eip.public_ip}"]
}