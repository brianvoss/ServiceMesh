variable "environment" {
  default = "prod"
}

variable "service" {
  default = "app"
}