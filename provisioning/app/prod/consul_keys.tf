provider "consul" {
  address    = "${data.terraform_remote_state.base.bastion_public_ip}:8500"
  datacenter = "${data.terraform_remote_state.base.consul_domain}"
  scheme     = "http"
}

resource "consul_key_prefix" "app_settings" {
  datacenter = "${data.terraform_remote_state.base.consul_domain}"

  path_prefix = "app/data_warehouse/"

  subkeys = {
    "db_host"     = "${data.terraform_remote_state.data.redshift_cluster_address}"
    "db_port"     = "${data.terraform_remote_state.data.redshift_database_port}"
    "db_database" = "${data.terraform_remote_state.data.redshift_database_name}"
    "db_username" = "${data.terraform_remote_state.data.redshift_database_username}"
    "db_password" = "${data.terraform_remote_state.data.redshift_database_password}"
  }
}