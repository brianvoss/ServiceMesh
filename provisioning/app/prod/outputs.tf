output "web_address" {
  value = "${aws_route53_record.traefik.fqdn}"
}