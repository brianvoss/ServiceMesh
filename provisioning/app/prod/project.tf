variable "team" {
	default = "acqu"
}

variable "service" {
	default = "sitemaps"
}

variable "product" {
	default = "Sitemap Generator"
}

variable "owner" {
	default = "growth-eng@trulia.com"
}

variable "description" {
	default = "Trulia Sitemap Generation Service"
}