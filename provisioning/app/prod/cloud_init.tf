
##
 # Shared Configs
##

# users and groups
data "template_file" "users_and_groups" {
  template = "${file("${path.module}/../templates/cloud-config/users_and_groups")}"
}

# ssh authorized keys
data "template_file" "ssh_authorized_keys" {
  template = "${file("${path.module}/../templates/cloud-config/ssh_keys")}"

  vars {
    keys = "${data.terraform_remote_state.vpc.bvoss_pubkey},${data.terraform_remote_state.vpc.automation_pubkey}"
  }
}

# default environment file
data "template_file" "environment_file" {
  template = "${file("${path.module}/../templates/cloud-config/environment_file")}"

  vars {
    CONSUL_MASTER     = "${data.terraform_remote_state.orchestration.bastion_internal_ip}"
    CONSUL_DOMAIN     = "${data.terraform_remote_state.orchestration.consul_domain}"
    CONSUL_DATACENTER = "${data.terraform_remote_state.orchestration.consul_datacenter}"
    environment       = "${var.environment}"
    service           = "${service}"
    role              = "app"
  }
}

##
 # App Configs
##

# first_boot_commands file
data "template_file" "app_first_boot_commands" {
  template = "${file("${path.module}/../templates/cloud-config/first_boot_commands")}"

  vars {
    commands = "${join(",", list("pwd",
    "chmod -r consul:consul /opt/consul"))}"
  }
}

# app consul agent config
data "template_file" "app_consul_agent" {
  template = "${file("${path.module}/../templates/cloud-config/consul_agent")}"

  vars {
    CONSUL_MASTER = "${data.terraform_remote_state.orchestration.bastion_internal_ip}"
    CONSUL_DATACENTER = "${data.terraform_remote_state.orchestration.consul_datacenter}"
    environment = "${var.environment}"
    service = "app"
    address = "0.0.0.0"
    health_check_path = "/"
    port = "80"
  }
}

# consul k/v config
data "template_file" "app_consul_template_key_values" {
  template = "${file("${path.module}/../../templates/cloud-config/consul_template")}"

  vars {
    CONSUL_MASTER = "${data.terraform_remote_state.orchestration.bastion_internal_ip}"
    KEY_ROOT = "/"
    output_file = "/etc/app/consul_config.json"
    command = "/bin/true"
    command_timeout = "1s"
  }
}

# app config
data "template_file" "app" {
  template = "${file("${path.module}/../../templates/cloud-config/blink_app")}"
}

# Render a multi-part cloudinit config making use of the part
# above, and other source files
data "template_cloudinit_config" "app" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "cloud-init-output.cfg"
    content_type = "text/cloud-config"
    content      = "output : { all : '|tee -a /var/log/cloud-init-output.log' }"
  }

  # users and groups
  part {
    filename     = "users_and_groups.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.users_and_groups.rendered}"
  }

  # ssh authorized keys
  part {
    filename     = "ssh_authorized_keys.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.ssh_authorized_keys.rendered}"
  }

  # consul
  part {
    filename     = "consul_agent.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.app_consul_agent.rendered}"
  }

  # environment file
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "environment_file.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.environment_file.rendered}"
  }

  # app
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "app.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.app.rendered}"
  }

  # first boot commands file
  part {
    filename     = "first_boot_cmds.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.first_boot_commands.rendered}"
  }

  # consul template key values
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "consul_template_key_values.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.app_consul_template_key_values.rendered}"
  }
}

##
 # Traefik Config
##

# traefik consul agent config
data "template_file" "traefik_consul_agent" {
  template = "${file("${path.module}/../../templates/cloud-config/consul_agent")}"

  vars {
    CONSUL_MASTER = "${data.terraform_remote_state.orchestration.bastion_internal_ip}"
    CONSUL_DATACENTER = "${data.terraform_remote_state.orchestration.consul_datacenter}"
    service = "traefik"
    environment = "${var.environment}"
    address = ""
    health_check_path = "/dashboard/"
    port = "8088"
  }
}

# traefik config
data "template_file" "traefik" {
  template = "${file("${path.module}/../../templates/cloud-config/traefik")}"

  vars {
    CONSUL_MASTER = "${data.terraform_remote_state.orchestration.bastion_internal_ip}"
    environment = "${var.environment}"
  }
}

# Render a multi-part cloudinit config making use of the part
# above, and other source files
data "template_cloudinit_config" "traefik" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "cloud-init-output.cfg"
    content_type = "text/cloud-config"
    content      = "output : { all : '|tee -a /var/log/cloud-init-output.log' }"
  }

  # users and groups
  part {
    filename     = "users_and_groups.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.users_and_groups.rendered}"
  }

  # ssh authorized keys
  part {
    filename     = "ssh_authorized_keys.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.ssh_authorized_keys.rendered}"
  }

  # consul
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "consul_agent.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.traefik_consul_agent.rendered}"
  }

  # environment file
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "environment_file.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.environment_file.rendered}"
  }

  # traefik
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "traefik.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.traefik.rendered}"
  }

  # consul template key values
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "consul_template_key_values.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.app_consul_template_key_values.rendered}"
  }
}