resource "aws_eip" "traefik-eip" {
  instance = "${aws_instance.traefik.id}"
  vpc = true
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = "${aws_instance.traefik.id}"
  allocation_id = "${aws_eip.traefik-eip.id}"
}