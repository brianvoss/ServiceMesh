module "redshift" {
  source  = "github.com/terraform-community-modules/tf_aws_redshift"

  # Redshift Cluster Inputs
  cluster_identifier      = "${var.service}-${var.environment}"
  cluster_node_type       = "ds1.8xlarge"
  cluster_number_of_nodes = "13"

  cluster_database_name   = "${var.redshift_database_name}"
  cluster_master_username = "${var.redshift_database_username}"
  cluster_master_password = "${var.redshift_database_password}"

  # Group parameters
  wlm_json_configuration     = "[{\"query_concurrency\": 5}]"

  # DB Subnet Group Inputs
  subnets         = ["${data.terraform_remote_state.base.public_subnets}", "${data.terraform_remote_state.base.private_subnets}"]
  redshift_vpc_id = "${data.terraform_remote_state.base.vpc_id}"
  private_cidr    = "${data.terraform_remote_state.base.vpc_cidr_block}"

  # IAM Roles
  cluster_iam_roles = [
    "arn:aws:iam::672698177022:role/RedshiftDefault"
  ]
}