variable "environment" {
  default = "prod"
}

variable "service_prefix" {
  default = "data"
}

variable "redshift_database_username" {
  default = "trulia"
}

variable "redshift_database_password" {
  default = "Turn0nthelights"
}

variable "redshift_database_port" {
  default = "5439"
}

variable "redshift_database_name" {
  default = "listingdata"
}