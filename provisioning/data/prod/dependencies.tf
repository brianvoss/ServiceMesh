data "terraform_remote_state" "core" {
  backend = "s3"

  config {
    bucket = "acqu-terraform-state"
    key    = "sitemaps/core/prod/terraform"
    region = "us-west-2"
  }
}