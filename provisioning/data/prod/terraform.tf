terraform {
  backend "s3" {
    bucket = "acqu-terraform-state"
    key    = "sitemaps/data/prod/terraform"
    region = "us-west-2"
  }
}