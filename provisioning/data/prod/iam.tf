#resource "aws_iam_role" "glue_service_role" {
#  name   = "${var.environment}-glue_service_role"
#  assume_role_policy = assume_role_policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Action": "sts:AssumeRole",
#      "Principal": {
#        "Service": "glue.amazonaws.com"
#      },
#      "Effect": "Allow",
#      "Sid": ""
#    }
#  ]
#}
#EOF
#}
#
#data "aws_iam_policy_document" "glue_service_policy" {
#
#  statement {
#  	effect = "Allow"
#  	actions = [
#      "glue:*",
#      "s3:GetBucketLocation",
#      "s3:ListBucket",
#      "s3:ListAllMyBuckets",
#      "s3:GetBucketAcl",
#      "ec2:DescribeVpcEndpoints",
#      "ec2:DescribeRouteTables",
#      "ec2:CreateNetworkInterface",
#      "ec2:DescribeNetworkInterfaces",
#      "ec2:DescribeSecurityGroups",
#      "ec2:DescribeSubnets",
#      "ec2:DescribeVpcAttribute",
#      "iam:ListRolePolicies",
#      "iam:GetRole",
#      "iam:GetRolePolicy"
#  	]
#  	resources = [
#  		"*"
#  	]
#  }
#
#  statement {
#      effect = "Allow",
#      actions = [
#          "s3:CreateBucket"
#      ],
#      resources = [
#          "arn:aws:s3:::aws-glue-*"
#      ]
#  },
#  statement {
#      effect = "Allow",
#      actions = [
#          "s3:GetObject",
#          "s3:PutObject",
#          "s3:DeleteObject"				
#      ],
#      resources = [
#          "arn:aws:s3:::aws-glue-*/*",
#          "arn:aws:s3:::*/*aws-glue-*/*"
#      ]
#  },
#  statement {
#      effect = "Allow",
#      actions = [
#          "s3:GetObject"
#      ],
#      resources = [
#          "arn:aws:s3:::crawler-public*",
#          "arn:aws:s3:::aws-glue-*"
#      ]
#  },
#  statement {
#      effect = "Allow",
#      actions = [
#          "logs:CreateLogGroup",
#          "logs:CreateLogStream",
#          "logs:PutLogEvents"
#      ],
#      resources = [
#          "arn:aws:logs:*:*:/aws-glue/*"
#      ]
#  },
#  statement {
#	  effect = "Allow",
#	  actions = [
#	    "ec2:CreateTags",
#	    "ec2:DeleteTags",
#	    "ec2:DeleteNetworkInterface"
#	  ],
#	  condition {
#	    test = "StringLike"
#	    variable = "aws:TagKeys"
#	    values = [
#	      "aws-glue-service-resource"
#	    ]
#	  }
#	  resources = [
#	    "arn:aws:ec2:*:*:network-interface/*",
#	    "arn:aws:ec2:*:*:security-group/*",
#	    "arn:aws:ec2:*:*:instance/*"
#	  ]
#  }
#}
#
#resource "aws_iam_role" "glue_notebook_role" {
#  name   = "${var.environment}-glue_notebook_role"
#  assume_role_policy = "${data.aws_iam_policy_document.glue_notebook_policy.json}"
#}
#
#data "aws_iam_policy_document" "glue_notebook_policy" {
#   statement {  
#      effect = "Allow",
#      actions = [  
#         "glue:CreateDatabase",
#         "glue:CreatePartition",
#         "glue:CreateTable",
#         "glue:DeleteDatabase",
#         "glue:DeletePartition",
#         "glue:DeleteTable",
#         "glue:GetDatabase",
#         "glue:GetDatabases",
#         "glue:GetPartition",
#         "glue:GetPartitions",
#         "glue:GetTable",
#         "glue:GetTableVersions",
#         "glue:GetTables",
#         "glue:UpdateDatabase",
#         "glue:UpdatePartition",
#         "glue:UpdateTable",
#         "glue:CreateBookmark",
#         "glue:GetBookmark",
#         "glue:UpdateBookmark",
#         "glue:GetMetric",
#         "glue:PutMetric",
#         "glue:CreateConnection",
#         "glue:CreateJob",
#         "glue:DeleteConnection",
#         "glue:DeleteJob",
#         "glue:GetConnection",
#         "glue:GetConnections",
#         "glue:GetDevEndpoint",
#         "glue:GetDevEndpoints",
#         "glue:GetJob",
#         "glue:GetJobs",
#         "glue:UpdateJob",
#         "glue:BatchDeleteConnection",
#         "glue:UpdateConnection",
#         "glue:GetUserDefinedFunction",
#         "glue:UpdateUserDefinedFunction",
#         "glue:GetUserDefinedFunctions",
#         "glue:DeleteUserDefinedFunction",
#         "glue:CreateUserDefinedFunction",
#         "glue:BatchGetPartition",
#         "glue:BatchDeletePartition",
#         "glue:BatchCreatePartition",
#         "glue:BatchDeleteTable",
#         "glue:UpdateDevEndpoint",
#         "s3:GetBucketLocation",
#         "s3:ListBucket",
#         "s3:ListAllMyBuckets",
#         "s3:GetBucketAcl"
#      ],
#      resources = [  
#         "*"
#      ]
#   },
#   statement {  
#      effect = "Allow",
#      actions = [  
#         "s3:GetObject"
#      ],
#      resources = [  
#         "arn:aws:s3:::crawler-public*",
#         "arn:aws:s3:::aws-glue*"
#      ]
#   },
#   statement {  
#      effect = "Allow",
#      actions = [  
#         "s3:PutObject",
#         "s3:DeleteObject"			
#      ],
#      resources = [  
#         "arn:aws:s3:::aws-glue*"
#      ]
#   },
#   statement {
#      effect = "Allow",
#      actions = [  
#         "ec2:CreateTags",
#         "ec2:DeleteTags"
#      ],
#      condition {  
#         test     = "StringLike"  
#         variable = "aws:TagKeys"
#         values = [  
#            "aws-glue-service-resource"
#         ]
#      }
#      resources = [  
#         "arn:aws:ec2:*:*:network-interface/*",
#         "arn:aws:ec2:*:*:security-group/*",
#         "arn:aws:ec2:*:*:instance/*"
#      ]
#   }
#}