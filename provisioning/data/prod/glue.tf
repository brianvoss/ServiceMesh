
# Glue requests from the VPC network

resource "aws_security_group" "open_glue_access" {
  name        = "${var.environment}-${var.service_prefix}-glue-access"
  description = "Allow glue access to the cluster: ${module.redshift.redshift_cluster_id}"
  vpc_id      = "${data.terraform_remote_state.base.vpc_id}"

  tags {
    Name = "${var.environment}-${var.service_prefix}-glue-access"
  }
}

# Keep rules separated to not recreate the cluster when deleting/adding rules
resource "aws_security_group_rule" "allow_all_inbound_glue" {
  type = "ingress"

  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  cidr_blocks = ["${data.terraform_remote_state.base.vpc_cidr_block}"]

  security_group_id = "${aws_security_group.open_glue_access.id}"
}

resource "aws_security_group_rule" "inbound_self_reference" {
  type = "ingress"

  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  self        = true

  security_group_id = "${aws_security_group.open_glue_access.id}"
}

resource "aws_security_group_rule" "outbound_self_reference" {
  type = "egress"

  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  self        = true

  security_group_id = "${aws_security_group.open_glue_access.id}"
}

resource "aws_security_group_rule" "allow_all_outbound_glue" {
  type = "egress"

  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.open_glue_access.id}"
}
