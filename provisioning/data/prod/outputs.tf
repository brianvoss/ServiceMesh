# Output the ID of the Redshift cluster
output "redshift_cluster_id" {
  value = "${module.redshift.redshift_cluster_id}"
}

# Output address (hostname) of the Redshift cluster
output "redshift_cluster_address" {
  value = "${module.redshift.redshift_cluster_address}"
}

# Output endpoint (hostname:port) of the Redshift cluster
output "redshift_cluster_endpoint" {
  value = "${module.redshift.redshift_cluster_endpoint}"
}

# Output host zone id
output "redshift_cluster_hosted_zone_id" {
  value = "${module.redshift.redshift_cluster_hosted_zone_id}"
}

# Output the ID of the Subnet Group
output "redshift_subnet_group_id" {
  value = "${module.redshift.subnet_group_id}"
}

# Output DB security group ID
output "redshift_security_group_id" {
  value = "${module.redshift.security_group_id}"
}

output "redshift_database_name" {
  value = "${var.redshift_database_name}"
}

output "redshift_database_username" {
  value = "${var.redshift_database_username}"
}

output "redshift_database_password" {
  value = "${var.redshift_database_password}"
}

output "redshift_database_port" {
  value = "${var.redshift_database_port}"
}