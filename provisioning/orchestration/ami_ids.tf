
data "aws_ami" "consul" {
  most_recent = true
  owners = ["self"]
  filter {
    name = "tag-key"
    values = ["role"]
  }
  filter {
    name = "tag-value"
    values = ["base"]
  }
}

data "aws_ami" "linkerd" {
  most_recent = true
  owners = ["self"]
  filter {
    name = "tag-key"
    values = ["role"]
  }
  filter {
    name = "tag-value"
    values = ["linkerd"]
  }
}

data "aws_ami" "zipkin" {
  most_recent = true
  owners = ["self"]
  filter {
    name = "tag-key"
    values = ["role"]
  }
  filter {
    name = "tag-value"
    values = ["zipkin"]
  }
}