
##
 # Consul Master Configs
##

# environment file
data "template_file" "consul_environment_file" {
  template = "${file("${path.module}/../templates/cloud-config/environment_file")}"

  vars {
    env_vars = "${join(",", list("PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "CONSUL_MASTER=127.0.0.1",
      "CONSUL_DOMAIN=${var.consul_domain}",
      "CONSUL_DATACENTER=${var.consul_datacenter}",
      "environment=${var.environment}",
      "service=${var.service}",
      "role=consul"))}"
  }
}

# consul master config
data "template_file" "consul_master" {
  template = "${file("${path.module}/../templates/cloud-config/consul_master")}"

  vars {
    CONSUL_DATACENTER = "${var.consul_datacenter}"
    CONSUL_DOMAIN = "${var.consul_domain}"
    environment = "${var.environment}"
    service = "consul"
    address = "0.0.0.0"
    health_check_path = "/"
    port = "8500"
  }
}

# Render a multi-part cloudinit config making use of the part
# above, and other source files
data "template_cloudinit_config" "consul_master" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "cloud-init-output.cfg"
    content_type = "text/cloud-config"
    content      = "output : { all : '|tee -a /var/log/cloud-init-output.log' }"
  }

  # users and groups
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "consul_user.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.consul_user.rendered}"
  }

  # ssh authorized keys
  part {
    filename     = "ssh_authorized_keys.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.ssh_authorized_keys.rendered}"
  }

  # environment file
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "environment_file.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.consul_environment_file.rendered}"
  }

  # consul master config
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "consul_master.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.consul_master.rendered}"
  }

  # common first boot
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "common_first_boot.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.first_boot_commands.rendered}"
  }
}