
variable "consul_datacenter" {
  default = "servicemesh"
}

variable "consul_domain" {
  default = "uxlabs.net"
}