
##
 # Shared Configs
##

# users and groups
data "template_file" "consul_user" {
  template = "${file("${path.module}/../templates/cloud-config/system_user")}"

  vars {
    username    = "consul"
    description = "Consul Daemon"
  }
}

# first_boot_commands file
data "template_file" "first_boot_commands" {
  template = "${file("${path.module}/../templates/cloud-config/first_boot_commands")}"

  vars {
    commands = "${join(",", list("pwd",
    "chown -R consul:consul /opt/consul"))}"
  }
}

# default environment file
data "template_file" "environment_file" {
  template = "${file("${path.module}/../templates/cloud-config/environment_file")}"

  vars {
    env_vars = "${join(",", list("PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "CONSUL_MASTER=${aws_instance.consul_master.private_ip}",
      "CONSUL_DOMAIN=${var.consul_domain}",
      "CONSUL_DATACENTER=${var.consul_datacenter}",
      "environment=${var.environment}",
      "service=${var.service}"))}"
  }
}

# ssh authorized keys
data "template_file" "ssh_authorized_keys" {
  template = "${file("${path.module}/../templates/cloud-config/ssh_keys")}"

  vars {
    keys = "${data.terraform_remote_state.vpc.bvoss_pubkey},${data.terraform_remote_state.vpc.automation_pubkey}"
  }
}