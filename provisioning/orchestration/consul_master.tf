
resource "aws_security_group" "consul_master" {
  name          = "${var.environment}-${var.service}-consul-master"
  description   = "Bastion Security Group"
  vpc_id        = "${data.terraform_remote_state.vpc.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.terraform_remote_state.vpc.admin_cidr_block}"]
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${data.terraform_remote_state.vpc.admin_cidr_block}"]
  }

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${data.terraform_remote_state.vpc.vpc_cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "udp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  # ICMP
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["${data.terraform_remote_state.vpc.admin_cidr_block}"]
  }

  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["${data.terraform_remote_state.vpc.admin_cidr_block}"]
  }
}

resource "aws_instance" "consul_master" {
  ami               = "${data.aws_ami.consul.id}"
  instance_type     = "t2.small"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  subnet_id         = "${data.terraform_remote_state.vpc.public_subnets[0]}"
  vpc_security_group_ids = ["${aws_security_group.consul_master.id}"]
  source_dest_check = false
  
  tags {
    Name = "${var.environment}-${var.service}-bastion"
  }

  user_data         = "${data.template_cloudinit_config.consul_master.rendered}"
}