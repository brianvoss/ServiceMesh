
##
 # Configs
##

# users and groups
data "template_file" "zipkin_user" {
  template = "${file("${path.module}/../templates/cloud-config/system_user")}"

  vars {
    username    = "zipkin"
    description = "Zipkin Daemon"
  }
}

# environment file
data "template_file" "zipkin_environment_file" {
  template = "${file("${path.module}/../templates/cloud-config/environment_file")}"

  vars {
    env_vars = "${join(",", list("PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "CONSUL_MASTER=${aws_instance.consul_master.private_ip}",
      "CONSUL_DOMAIN=${var.consul_domain}",
      "CONSUL_DATACENTER=${var.consul_datacenter}",
      "environment=${var.environment}",
      "service=${var.service}",
      "role=zipkin"))}"
  }
}

# first_boot_commands file
data "template_file" "zipkin_first_boot_commands" {
  template = "${file("${path.module}/../templates/cloud-config/first_boot_commands")}"

  vars {
    commands = "${join(",", list("pwd",
    "sudo mkdir /var/log/zipkin",
    "sudo chown zipkin:zipkin /var/log/zipkin"))}"
  }
}

# zipkin consul agent config
data "template_file" "zipkin_consul_agent" {
  template = "${file("${path.module}/../templates/cloud-config/consul_agent")}"

  vars {
    CONSUL_MASTER = "${aws_instance.consul_master.private_ip}"
    CONSUL_DATACENTER = "${var.consul_datacenter}"
    environment = "${var.environment}"
    service = "zipkin"
    address = ""
    health_check_path = "/zipkin/"
    port = "9411"
  }
}

# zipkin config
data "template_file" "zipkin" {
  template = "${file("${path.module}/templates/cloud-config/zipkin")}"

  vars {
    CONSUL_MASTER = "${aws_instance.consul_master.private_ip}"
    environment = "${var.environment}"
  }
}


# Render a multi-part cloudinit config making use of the part
# above, and other source files
data "template_cloudinit_config" "zipkin" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "cloud-init-output.cfg"
    content_type = "text/cloud-config"
    content      = "output : { all : '|tee -a /var/log/cloud-init-output.log' }"
  }

  # users and groups
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "consul_user.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.consul_user.rendered}"
  }
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "users_and_groups.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.zipkin_user.rendered}"
  }

  # ssh authorized keys
  part {
    filename     = "ssh_authorized_keys.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.ssh_authorized_keys.rendered}"
  }

  # consul
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "consul_agent.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.zipkin_consul_agent.rendered}"
  }

  # environment file
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "environment_file.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.zipkin_environment_file.rendered}"
  }

  # zipkin
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "zipkin.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.zipkin.rendered}"
  }

  # common first boot
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "common_first_boot.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.first_boot_commands.rendered}"
  }

  # zipkin first boot
  part {
    merge_type   = "list(append)+dict(recurse_array)+str()"
    filename     = "zipkin_first_boot.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.zipkin_first_boot_commands.rendered}"
  }
}