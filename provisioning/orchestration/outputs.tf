
output "environment" {
	value = "${var.environment}"
}

output "bastion_public_ip" {
  value = "${aws_eip.consul-master-eip.public_ip}"
}

output "bastion_internal_ip" {
	value = "${aws_instance.consul_master.private_ip}"
}

output "consul_address" {
  value = "${aws_route53_record.consul.fqdn}"
}

output "consul_datacenter" {
	value = "${var.consul_datacenter}"
}

output "consul_domain" {
	value = "${var.consul_domain}"
}

output "consul_cluster_name" {
	value = "${var.service}-${var.environment}"
}

output "linkerd_address" {
  value = "${aws_route53_record.linkerd.fqdn}"
}

output "linkerd_public_address" {
	value = "${aws_eip.linkerd-eip.public_ip}"
}

output "linkerd_private_address" {
	value = "${aws_instance.linkerd.private_ip}"
}

output "zipkin_address" {
  value = "${aws_route53_record.zipkin.fqdn}"
}

output "zipkin_public_address" {
	value = "${aws_eip.zipkin-eip.public_ip}"
}

output "zipkin_private_address" {
	value = "${aws_instance.zipkin.private_ip}"
}