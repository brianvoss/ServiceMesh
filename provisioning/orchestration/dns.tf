
data "aws_route53_zone" "uxlabs" {
  name         = "uxlabs.net."
  private_zone = false
}

resource "aws_route53_record" "consul" {
  zone_id = "${data.aws_route53_zone.uxlabs.zone_id}"
  name    = "consul.servicemesh.${data.aws_route53_zone.uxlabs.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.consul-master-eip.public_ip}"]
}

resource "aws_route53_record" "linkerd" {
  zone_id = "${data.aws_route53_zone.uxlabs.zone_id}"
  name    = "linkerd.servicemesh.${data.aws_route53_zone.uxlabs.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.linkerd-eip.public_ip}"]
}

resource "aws_route53_record" "zipkin" {
  zone_id = "${data.aws_route53_zone.uxlabs.zone_id}"
  name    = "zipkin.servicemesh.${data.aws_route53_zone.uxlabs.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.zipkin-eip.public_ip}"]
}