
resource "aws_eip" "consul-master-eip" {
  instance = "${aws_instance.consul_master.id}"
  vpc = true
}

resource "aws_eip" "linkerd-eip" {
  instance = "${aws_instance.linkerd.id}"
  vpc = true
}

resource "aws_eip" "zipkin-eip" {
  instance = "${aws_instance.zipkin.id}"
  vpc = true
}