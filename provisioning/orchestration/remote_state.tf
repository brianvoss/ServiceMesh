
terraform {
  backend "s3" {
    bucket = "servicemesh-terraform-state"
    key    = "servicemesh/orchestration/terraform"
    region = "us-west-2"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "servicemesh-terraform-state"
    key    = "servicemesh/vpc/terraform"
    region = "us-west-2"
  }
}