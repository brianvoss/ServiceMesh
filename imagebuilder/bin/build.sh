#!/bin/bash

PACKER=`which packer`
VAGRANT=`which vagrant`
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PARENT_DIR="$( cd "$( dirname "$CURRENT_DIR" )" && pwd )"

ALL_TARGETS="amazon-ebs docker"
ALL_ROLES="base php-app linkerd-router"

BUILD_TARGET=""
BUILD_ROLE=""

OUTPUT_DIR="$PARENT_DIR/dist"

DEBUG=false

ONLY=""

function parse_args() {
	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
	    -r|--role)
	    BUILD_ROLE="$2"
	    shift # past argument
	    ;;
	    -t|--target)
	    BUILD_TARGET="$2"
	    shift # past argument
	    ;;
	    -d|--debug)
	    DEBUG=true
	    shift # past argument
	    ;;
	    *)
	    echo "Unknown Option $key"
	    ;;
	esac
	shift # past argument or value
	done
}

parse_args "$@"

if [ $DEBUG == true ]; then
	echo "[** PARENT_DIR **]: $PARENT_DIR"
	echo "[** OUTPUT_DIR **]: $OUTPUT_DIR"
fi

# Default to all targets
if [ "$BUILD_TARGET" == "" ]; then
	BUILD_TARGET="$ALL_TARGETS"
	ONLY=""
else
	ONLY="-only=$BUILD_TARGET"
fi

# Default to all roles
if [ "$BUILD_ROLE" == "" ]; then
	BUILD_ROLE="$ALL_ROLES"
fi

# Make sure output directory exists
mkdir -p "$OUTPUT_DIR"

########################################
# Build Prep
########################################
echo "Build Summary"
echo "Role: $BUILD_ROLE"
echo "Target: $BUILD_TARGET"
export PACKER_LOG=1

for ROLE in $BUILD_ROLE; do
	SETUP_FILE="$PARENT_DIR/roles/$ROLE/scripts/before.sh"
	CLEANUP_FILE="$PARENT_DIR/roles/$ROLE/scripts/after.sh"
	BUILD_FILE="$PARENT_DIR/roles/$ROLE/packer.json"
	if [ $DEBUG == true ]; then
		echo "[** SETUP FILE **]: $SETUP_FILE"
		echo "[** BUILD FILE **]: $BUILD_FILE"
		echo "[** CLEANUP FILE **]: $CLEANUP_FILE"
	fi
	########################################
	# Build Setup
	########################################
	$SETUP_FILE $BUILD_TARGET
	########################################
	# BUILD
	########################################
	if [ $DEBUG == true ]; then
		echo "[** Build **]: Proceeding with Packer build"
	fi
	$PACKER build -force $ONLY --var "output_dir=$OUTPUT_DIR" --var "role_name=$ROLE" --var "base_dir=$PARENT_DIR/roles" "$BUILD_FILE"
	########################################
	# Build Cleanup
	########################################
	$CLEANUP_FILE $BUILD_TARGET
	echo "Finished building role $ROLE"
	echo ""
done