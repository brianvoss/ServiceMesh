#!/bin/bash

AWS_REGION=us-west-2

#AWS_SOURCE_AMI=ami-0a00ce72 # xenial us-west-2
AWS_SOURCE_AMI=ami-20ad7858 # base image

export AWS_SOURCE_AMI=${AWS_SOURCE_AMI}
export AWS_REGION=${AWS_REGION}