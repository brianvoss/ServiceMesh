#!/bin/bash
sudo apt-get install -y unzip

cd ~/ && curl https://releases.hashicorp.com/consul/1.0.0/consul_1.0.0_linux_amd64.zip -o consul_1.0.0.zip
unzip consul_1.0.0.zip

sudo mv consul /usr/local/bin/
sudo mkdir -p /opt/consul/data
sudo mkdir -p /opt/consul/ui
sudo mkdir -p /etc/consul
sudo mkdir -p /var/log/consul