#!/bin/bash
sudo apt-get install -y software-properties-common
sudo apt-add-repository -y ppa:webupd8team/java
sudo apt-get update

# Auto accept the Oracle license
echo debconf shared/accepted-oracle-license-v1-1 select true | \
  sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
  sudo debconf-set-selections

sudo apt-get install -y --allow-unauthenticated oracle-java8-installer

curl -L 'https://search.maven.org/remote_content?g=io.zipkin.java&a=zipkin-server&v=LATEST&c=exec' -o zipkin.jar
sudo mv zipkin.jar /usr/local/bin/zipkin.jar

sudo chmod +x /usr/local/bin/zipkin.jar