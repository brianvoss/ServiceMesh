#!/bin/bash
set -e

BUILD_TYPE=$1
RETURN=1 # default return
VAGRANT=`which vagrant`

# Directories
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROLE_DIR="$( cd "$( dirname "$CURRENT_DIR" )" && pwd )"
ROLES_DIR="$( cd "$( dirname "$ROLE_DIR" )" && pwd )"
BASE_DIR="$( cd "$( dirname "$ROLES_DIR" )" && pwd )"
OUTPUT_DIR="$BASE_DIR/dist"

echo "clean up for build type $BUILD_TYPE"

########################################
# POST BUILD
########################################

for TYPE in $BUILD_TYPE; do
  echo "Running $TYPE pre build set-up"
  case "$TYPE" in
    docker)
      # noop
      ;;
    amazon-ebs)
      # noop
      ;;
  esac
done