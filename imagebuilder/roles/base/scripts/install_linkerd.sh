#!/bin/bash

sudo apt-get install -y software-properties-common
sudo apt-add-repository -y ppa:webupd8team/java
sudo apt-get update

# Auto accept the Oracle license
echo debconf shared/accepted-oracle-license-v1-1 select true | \
  sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
  sudo debconf-set-selections

sudo apt-get install -y --allow-unauthenticated oracle-java8-installer

curl -L https://github.com/linkerd/linkerd/releases/download/1.1.2/linkerd-1.1.2-exec -o linkerd
sudo mv linkerd /usr/local/bin/linkerd
sudo chmod +x /usr/local/bin/linkerd