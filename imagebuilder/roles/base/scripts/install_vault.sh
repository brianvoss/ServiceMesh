#!/bin/bash

sudo apt-get install -y unzip

VAULT_URL='https://releases.hashicorp.com/vault/0.6.4'
FILENAME='vault_0.6.4_linux_amd64.zip'

cd ~/ && curl -o $FILENAME "$VAULT_URL/$FILENAME"
cd ~/ && unzip $FILENAME

sudo mv vault /usr/local/bin/