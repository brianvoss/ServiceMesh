#!/bin/bash

sudo apt-get install -y scala
curl -L https://github.com/linkerd/linkerd/releases/download/1.1.2/namerd-1.1.2-exec -o namerd
curl -L https://github.com/linkerd/namerctl/releases/download/0.8.6/namerctl_linux_amd64 -o namerctl
sudo mv namerd /usr/local/bin/namerd
sudo mv namerctl /usr/local/bin/namerctl

sudo chmod +x /usr/local/bin/namerd
sudo chmod +x /usr/local/bin/namerctl