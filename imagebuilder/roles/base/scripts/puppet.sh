#!/bin/bash

wget https://apt.puppetlabs.com/puppetlabs-release-pc1-trusty.deb
sudo dpkg -i puppetlabs-release-pc1-trusty.deb
sudo apt-get update

sudo apt-get install puppet-agent

sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true