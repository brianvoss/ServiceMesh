#!/bin/bash
sudo apt-get update
# Add PPA for PHP 7
sudo apt-get install -y python-software-properties software-properties-common unzip git
sudo LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
sudo apt-get update

# Install PHP
sudo apt-get install -y php7.1 php7.1-cli php7.1-dev php7.1-fpm php7.1-json php7.1-mbstring php7.1-bcmath php7.1-pgsql unzip zlib1g-dev build-essential nghttp2 libnghttp2-dev

sudo pecl install grpc

echo 'extension=grpc.so' | sudo tee /etc/php/7.1/mods-available/grpc.ini
cd /etc/php/7.1/cli/conf.d && sudo ln -s /etc/php/7.1/mods-available/grpc.ini

# Install composer
cd ~/
EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
RESULT=$?
rm composer-setup.php
sudo mv composer.phar /usr/local/bin/composer
exit $RESULT