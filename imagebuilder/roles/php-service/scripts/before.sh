#!/bin/bash
set -e

BUILD_TYPE=$1
RETURN=1 # default return
VAGRANT=`which vagrant`

# Directories
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROLE_DIR="$( cd "$( dirname "$CURRENT_DIR" )" && pwd )"
ROLES_DIR="$( cd "$( dirname "$ROLE_DIR" )" && pwd )"
BASE_DIR="$( cd "$( dirname "$ROLES_DIR" )" && pwd )"
OUTPUT_DIR="$BASE_DIR/dist"

echo "setup for build type $BUILD_TYPE"

########################################
# POST BUILD
########################################

for TYPE in $BUILD_TYPE; do
  echo "Running $TYPE pre build set-up"
  case "$TYPE" in
    docker)
      # noop
      ;;
    amazon-ebs)
      source "$BASE_DIR/bin/set_env.sh"
      if [ ! -f "$ROLE_DIR/source/opt/sitemap_generator.tar" ]; then
        cd $ROLE_DIR/source/opt/ && git clone ssh://git@stash.sv2.trulia.com/web/sitemap-generator.git
        cd $ROLE_DIR/source/opt/sitemap-generator/ && composer install --ignore-platform-reqs
        cd $ROLE_DIR/source/opt/sitemap-generator/ && tar -czvf $ROLE_DIR/source/opt/sitemap-generator.tar .
        rm -rf $ROLE_DIR/source/opt/sitemap-generator/
      fi
      ;;
  esac
done