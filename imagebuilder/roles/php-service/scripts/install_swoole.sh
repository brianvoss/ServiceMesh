#!/bin/bash

# Install Swoole
cd ~/
git clone https://github.com/swoole/swoole-src.git
cd swoole-src
phpize
./configure --enable-http2 --enable-openssl
make clean && make && sudo make install

echo 'extension=swoole.so' | sudo tee /etc/php/7.1/mods-available/swoole.ini
cd /etc/php/7.1/cli/conf.d && sudo ln -s /etc/php/7.1/mods-available/swoole.ini